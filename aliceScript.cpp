//#include <gmp.h> /* for arithmetic on large integers */
//#include <stdio.h>
//#include <stdlib.h>
#include <iostream>
using std::cin;
using std::cout;
#include <NTL/ZZ_pX.h>
using namespace NTL;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include <string>
using std::string;


/*storage for the parameters*/
ZZ q, p, g, a, A, x, h, Ah;

/* you could use something like this to read the parameters
 * from a file.  I have placed the values q,p,g (in that order)
 * which were posted on piazza in the file ../params-q-p-g
 * NOTE: are written in base 10 for convenience. */
int readParams()
{
	ifstream fin("params-q-p-g");
	if (!fin.good()) {
		fprintf(stderr, "couldn't open parameter file.\n");
		return 1;
	}
	/* NOTE: q,p,g declared above at global scope. */
	fin >> q >> p >> g;
	fin.close();
	/* Let's perform a quick sanity check: are values which are
	 * supposed to be prime actually so?  Does q in fact divide
	 * the order of the multiplicative group mod p? */
	if (!ProbPrime(q)) {
		fprintf(stderr, "q not prime!\n");
		return -1;
	}
	if (!ProbPrime(p)) {
		fprintf(stderr, "p not prime!\n");
		return -1;
	}
	if ((p-1)%q != 0) {
		fprintf(stderr, "q does not divde p-1!\n");
		return -1;
	}
	if ((p-1)%(q*q) == 0) {
		fprintf(stderr, "q^2 divides p-1!\n");
		return -1;
	}
	/* lastly, let's check on the generator: */
	if (PowerMod(g,(p-1)/q,p) == 1) {
		fprintf(stderr, "g does not generate subgroup of size q!\n");
		return -1;
	}

	// computing the secret key a, and the public key A = g^a.
	while (a == 0){
		a = conv<ZZ>(RandomBnd(p));
	}
	PowerMod(A, g, a, p);


	/* NOTE: we can also set the modulus for the ZZ_p datatypes.
	 * This way you never have to perform any explicit reductions
	 * when doing arithmetic, e.g., X*Y means X*Y % p for X,Y of
	 * type ZZ_p. */
	ZZ_p::init(p);
	/* NOTE: for secret sharing, you could also use this, but you
	 * will likely want to use parameter q instead. */
	return 0;
}

/* you can (and should!) use something like this to initialize
 * the state of NTL's internal random number generator.  Also
 * note that unlike the rand() function from libc, NTL random
 * values are of cryptographic quality. */
void initNTLRandom()
{
	FILE* frand = fopen("/dev/urandom","rb");
	unsigned char seed[32];
	fread(seed,1,32,frand);
	fclose(frand);
	SetSeed(seed,32);
}



int main(){
	readParams();
	initNTLRandom();

  	/* Obtain from Bob the value of h which was chosen randomly form the group <G> */
	ZZ h;
	do {
		string string_h = "";
		cout << "Please enter the value of h: \n";
		cin >> string_h;
		const char* C_str_h =  string_h.c_str();
		h = conv<ZZ>(C_str_h);
	} while (h > q);
  	/* computing Ah = h^a mod p */
	PowerMod(Ah, h, a, p);

  	// computing the random value x in Interger mod q.
	while (x == 0){
		x = conv<ZZ>(RandomBnd(q));
	}
  	ZZ Xg, Xh;
	PowerMod(Xg, g, x, p);           /* compute Xg */
  	PowerMod(Xh, h, x, p);           /* compute Xh */

  	cout << "The value of Xg is: " << Xg << "\n";
  	cout << "The value of Xh is: " << Xh << "\n";

  	/* Obtain from Bob the value of c which was chosen randomly form Integers mod q. */
  	ZZ c;
	do {
		string string_c = "";
		cout << "Please enter the value of c: \n";
		cin >> string_c;
		const char* C_str_c =  string_c.c_str();
		h = conv<ZZ>(C_str_c);
	} while (c > q);

    /* computing the value of k = c*a + x mod q and send it to Bob */
  	ZZ k;
  	AddMod(k, MulMod(c, a, q), x, q);
  	cout << "the value of k is: " << k << "\n";


  	return 0;
}

