/* simple program using GMP to test for primality.  Prints output
 * of mpz_probab_prime_p to stdout for each integer read on stdin.
 * NOTE: 2 means "def prime" and 1 means "probably prime", but with
 * 10 trials, "probably" is all but certain.  See Pomerance et al.:
 * http://www.ams.org/mcom/1993-61-203/S0025-5718-1993-1189518-9/ */
#include <stdio.h>
#include <gmp.h>

#define ISPRIME(x) mpz_probab_prime_p(x,10)
/* macro to declare a new long integer: */
#define NEWZ(x) mpz_t x; mpz_init(x)
#define BYTES2Z(x,buf,len) mpz_import(x,len,-1,1,0,0,buf)
#define Z2BYTES(buf,len,x) mpz_export(buf,&len,-1,1,0,0,x)

int main() {
	NEWZ(p);
	while (gmp_scanf("%Zd",p) == 1) {
		printf("%i\n",ISPRIME(p));
	}
	return 0;
}
