//#include <gmp.h> /* for arithmetic on large integers */
//#include <stdio.h>
//#include <stdlib.h>
#include <iostream>
using std::cin;
using std::cout;
#include <NTL/ZZ_pX.h>
using namespace NTL;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include <string>
using std::string;


/*storage for the parameters*/
ZZ q, p, g, a, A, r, h, Ah;

/* you could use something like this to read the parameters
 * from a file.  I have placed the values q,p,g (in that order)
 * which were posted on piazza in the file ../params-q-p-g
 * NOTE: are written in base 10 for convenience. */
int readParams()
{
	ifstream fin("params-q-p-g");
	if (!fin.good()) {
		fprintf(stderr, "couldn't open parameter file.\n");
		return 1;
	}
	/* NOTE: q,p,g declared above at global scope. */
	fin >> q >> p >> g;
	fin.close();
	/* Let's perform a quick sanity check: are values which are
	 * supposed to be prime actually so?  Does q in fact divide
	 * the order of the multiplicative group mod p? */
	if (!ProbPrime(q)) {
		fprintf(stderr, "q not prime!\n");
		return -1;
	}
	if (!ProbPrime(p)) {
		fprintf(stderr, "p not prime!\n");
		return -1;
	}
	if ((p-1)%q != 0) {
		fprintf(stderr, "q does not divde p-1!\n");
		return -1;
	}
	if ((p-1)%(q*q) == 0) {
		fprintf(stderr, "q^2 divides p-1!\n");
		return -1;
	}
	/* lastly, let's check on the generator: */
	if (PowerMod(g,(p-1)/q,p) == 1) {
		fprintf(stderr, "g does not generate subgroup of size q!\n");
		return -1;
	}

	// obtaining the secret key a, and a random value r < p which will be used to compute h.
	while (a == 0 && r == 0){
		a = conv<ZZ>(RandomBnd(p));
		r = conv<ZZ>(RandomBnd(p));
	}
	PowerMod(A, g, a, p);  /* computing the public key A */
	/* computing Ah = h^a mod p */
	PowerMod(h, g, r, p);
	PowerMod(Ah, h, a, p);

	/* NOTE: we can also set the modulus for the ZZ_p datatypes.
	 * This way you never have to perform any explicit reductions
	 * when doing arithmetic, e.g., X*Y means X*Y % p for X,Y of
	 * type ZZ_p. */
	ZZ_p::init(p);
	/* NOTE: for secret sharing, you could also use this, but you
	 * will likely want to use parameter q instead. */
	return 0;
}


/* you can (and should!) use something like this to initialize
 * the state of NTL's internal random number generator.  Also
 * note that unlike the rand() function from libc, NTL random
 * values are of cryptographic quality. */
void initNTLRandom()
{
	FILE* frand = fopen("/dev/urandom","rb");
	unsigned char seed[32];
	fread(seed,1,32,frand);
	fclose(frand);
	SetSeed(seed,32);
}




int main(){
	readParams();
	initNTLRandom();

	/* Obtain from the user the values of Xg = g^x mod p, and Xh = h^x mod p. make sure that both are less than p */
	ZZ Xg, Xh;
	do {
		string string_Xg = "", string_Xh = "";
		cout << "Please enter the value of Xg = g^x mod p: \n";
		cin >> string_Xg;
		const char* C_str_Xg =  string_Xg.c_str();
		Xg = conv<ZZ>(C_str_Xg);
		cout << "Please enter the value of Xh = h^x mod p: \n";
		cin >> string_Xh;
		const char* C_str_Xh =  string_Xh.c_str();
		Xh = conv<ZZ>(C_str_Xh);
	} while (Xg > p || Xh > p);

	/* send to the user the random value c from Intergers mod q. */
	ZZ c;
	while (c == 0){
		c = conv<ZZ>(RandomBnd(q));
	}
	cout << "The value c which is selected randomly from Intergers mod q is: " << c << "\n";

	/* Obtain from the user the value of k = c*a + x   mod q */
	ZZ k;
	do {
		string string_k = "";
		cout << "Please enter the value of k = c*a + x mod q: \n";
		cin >> string_k;
		const char* C_str_k =  string_k.c_str();
		k = conv<ZZ>(C_str_k);
	} while (k > q);

	/* verify that (g^k = A^c * Xg) && (h^k = Ah^c *Xh) */
	bool condition1 = false, condition2 = false;
	if (PowerMod(g, k, q) == MulMod(PowerMod(A, c, q), Xg, q) ) condition1 = true;
	if (PowerMod(h, k, q) == MulMod(PowerMod(Ah, c, q), Xh, q) ) condition2 = true;



	/* Output to the user if authenticated or not */
	if (condition1 && condition2){
		cout << "Welcome Alice.\n";
	}else {
		cout << "You're not Alice! Who are you?\n";
	}

	return 0;
}
