#include <stdio.h>
#include <string.h>
#include <openssl/sha.h>
#include <iostream>
using std::cin;
using std::cout;
#include <NTL/ZZ_pX.h>
using namespace NTL;
#include <fstream>
using std::ifstream;
using std::ofstream;
#include <string>
using std::string;
#include <vector>
using std::vector;


/*storage for the parameters*/
ZZ q, p, g, h, secretKey, gPublicKey, hPublicKey ;

/* you could use something like this to read the parameters
 * from a file.  I have placed the values q,p,g (in that order)
 * which were posted on piazza in the file ../params-q-p-g
 * NOTE: are written in base 10 for convenience. */
int readParams()
{
	ifstream fin("params-q-p-g-h");
	if (!fin.good()) {
		fprintf(stderr, "couldn't open parameter file.\n");
		return 1;
	}
	/* NOTE: q,p,g,h declared above at global scope. */
	fin >> q >> p >> g >> h;
	fin.close();
	/* Let's perform a quick sanity check: are values which are
	 * supposed to be prime actually so?  Does q in fact divide
	 * the order of the multiplicative group mod p? */
	if (!ProbPrime(q)) {
		fprintf(stderr, "q not prime!\n");
		return -1;
	}
	if (!ProbPrime(p)) {
		fprintf(stderr, "p not prime!\n");
		return -1;
	}
	if ((p-1)%q != 0) {
		fprintf(stderr, "q does not divde p-1!\n");
		return -1;
	}
	if ((p-1)%(q*q) == 0) {
		fprintf(stderr, "q^2 divides p-1!\n");
		return -1;
	}
	/* lastly, let's check on the generators: */
	if (PowerMod(g,(p-1)/q,p) == 1) {
		fprintf(stderr, "g does not generate subgroup of size q!\n");
		return -1;
	}
	if (PowerMod(h,(p-1)/q,p) == 1) {
		fprintf(stderr, "h does not generate subgroup of size q!\n");
		return -1;
	}
	/* computing the secret key a, and the public keys for the generators g and h as:
	 * gPublicKey = g^secretKey , hPublicKey = h^secretKey.
	 */
	while (secretKey == 0){
		secretKey = conv<ZZ>(RandomBnd(p));
	}
	PowerMod(gPublicKey, g, secretKey, p);
	PowerMod(hPublicKey, h, secretKey, p);


	/* NOTE: we can also set the modulus for the ZZ_p datatypes.
	 * This way you never have to perform any explicit reductions
	 * when doing arithmetic, e.g., X*Y means X*Y % p for X,Y of
	 * type ZZ_p. */
	ZZ_p::init(p);
	/* NOTE: for secret sharing, you could also use this, but you
	 * will likely want to use parameter q instead. */
	return 0;
}

/* you can (and should!) use something like this to initialize
 * the state of NTL's internal random number generator.  Also
 * note that unlike the rand() function from libc, NTL random
 * values are of cryptographic quality. */
void initNTLRandom()
{
	FILE* frand = fopen("/dev/urandom","rb");
	unsigned char seed[32];
	fread(seed,1,32,frand);
	fclose(frand);
	SetSeed(seed,32);
}


vector<ZZ> Prover(){
	/* vector oc big numers ZZ to hold the values that the prover will send to the verifier */
	vector<ZZ> result;
	result.clear();

	/* computing a random value r such that r is in the Zq */
	ZZ randomValue, commit_g, commit_h;
	while (randomValue == 0){
		randomValue = conv<ZZ>(RandomBnd(q));
	}
	/* compute the two commitments commit_g = g^r mod p, commit_h = h^r mod p */
	PowerMod(commit_g, g, randomValue, p);
	PowerMod(commit_h, h, randomValue, p);
	result.push_back(commit_g);
	result.push_back(commit_h);

	/* compute the challenge c for now will be random but i'll replace it with the output of the hash function SHA512
	 * challenge = H(gPublicKey, hPublicKey, commit_g, commit_h) don't forget to reduce mod q
	 * */
	/* create buffers to hold the byte sequences of the two commitements and the two logarithm */
	unsigned char commit_g_bytes[NumBytes(commit_g)];
	unsigned char commit_h_bytes[NumBytes(commit_h)];
	unsigned char gPublickey_bytes[NumBytes(gPublicKey)];
	unsigned char hPublickey_bytes[NumBytes(hPublicKey)];
	memset(commit_g_bytes, 0, NumBytes(commit_g));
	memset(commit_h_bytes, 0, NumBytes(commit_h));
	memset(gPublickey_bytes, 0, NumBytes(gPublicKey));
	memset(hPublickey_bytes, 0, NumBytes(hPublicKey));
	BytesFromZZ(commit_g_bytes, commit_g, NumBytes(commit_g));
	BytesFromZZ(commit_h_bytes, commit_h, NumBytes(commit_h));
	BytesFromZZ(gPublickey_bytes, gPublicKey, NumBytes(gPublicKey));
	BytesFromZZ(hPublickey_bytes, hPublicKey, NumBytes(hPublicKey));
	/* create a buffer to hold the concatenation of the two commitements and the two logarithms, concatenate after */
	unsigned char challenge_string[sizeof(commit_g_bytes)+sizeof(commit_h_bytes)+sizeof(gPublickey_bytes)+sizeof(hPublickey_bytes)];
	memcpy(challenge_string, commit_g_bytes, sizeof(commit_g_bytes));
	memcpy(challenge_string+NumBytes(commit_g), commit_h_bytes, sizeof(commit_h_bytes));
	memcpy(challenge_string+NumBytes(commit_g)+NumBytes(commit_h), gPublickey_bytes, sizeof(gPublickey_bytes));
	memcpy(challenge_string+NumBytes(commit_g)+NumBytes(commit_h)+NumBytes(gPublicKey), hPublickey_bytes, sizeof(hPublickey_bytes));
	/* compute the hash using SHA512 */
	unsigned char hash[64];
	SHA512((unsigned char*)challenge_string, sizeof(challenge_string), hash);
	/* convert the hashed challenge into ZZ and make sure it's less than q.*/
	ZZ challenge;
	ZZFromBytes(challenge, hash, 64);
	challenge = challenge % q;
	if (challenge >= q) cout<< "The challenge is greater than the prime q.\n";
	result.push_back(challenge);
	/*
	cout <<"Prover challenge after been hashed:\n";
	cout << challenge <<"\n\n";
	*/
	ZZ lastMessage;
	/* computing the value of lastMessage = r + challenge*secretKey mod(q)*/
	AddMod(lastMessage, randomValue, MulMod(challenge, secretKey, q), q);
	result.push_back(lastMessage);
	/* return to the verifier the vector (commit_g, commit_h, challenge, lastMessage)  */
	return result;
}


bool Verifier(const vector<ZZ>& proverMessage){
	bool result = false;
	/* 	the verifier will compute the value of the challenge himself using the hash function
	 *  and the values of the commitments from the prover as:
	 *  verifier_c = H(gPublicKey, hPublicKey, proverMessage[0], proverMessage[1]);
	 * 	and compare it to the value that the prover sent to him if they're equal then he'll continue.*/
	
	/* create buffers to hold the byte sequences of the two commitements and the two logarithm */
	unsigned char commit_g_bytes[NumBytes(proverMessage[0])];
	unsigned char commit_h_bytes[NumBytes(proverMessage[1])];
	unsigned char gPublickey_bytes[NumBytes(gPublicKey)];
	unsigned char hPublickey_bytes[NumBytes(hPublicKey)];
	memset(commit_g_bytes, 0, NumBytes(proverMessage[0]));
	memset(commit_h_bytes, 0, NumBytes(proverMessage[1]));
	memset(gPublickey_bytes, 0, NumBytes(gPublicKey));
	memset(hPublickey_bytes, 0, NumBytes(hPublicKey));
	BytesFromZZ(commit_g_bytes, proverMessage[0], NumBytes(proverMessage[0]));
	BytesFromZZ(commit_h_bytes, proverMessage[1], NumBytes(proverMessage[1]));
	BytesFromZZ(gPublickey_bytes, gPublicKey, NumBytes(gPublicKey));
	BytesFromZZ(hPublickey_bytes, hPublicKey, NumBytes(hPublicKey));
	/* create a buffer to hold the concatenation of the two commitements and the two logarithms, concatenate after */
	unsigned char challenge_string[sizeof(commit_g_bytes)+sizeof(commit_h_bytes)+sizeof(gPublickey_bytes)+sizeof(hPublickey_bytes)];
	memcpy(challenge_string, commit_g_bytes, sizeof(commit_g_bytes));
	memcpy(challenge_string+NumBytes(proverMessage[0]), commit_h_bytes, sizeof(commit_h_bytes));
	memcpy(challenge_string+NumBytes(proverMessage[0])+NumBytes(proverMessage[1]), gPublickey_bytes, sizeof(gPublickey_bytes));
	memcpy(challenge_string+NumBytes(proverMessage[0])+NumBytes(proverMessage[1])+NumBytes(gPublicKey), hPublickey_bytes, sizeof(hPublickey_bytes));
	/* compute the hash using SHA512 */
	unsigned char hash[64];
	SHA512((unsigned char*)challenge_string, sizeof(challenge_string), hash);
	/* convert the hashed challenge into ZZ and make sure it's less than q.*/
	ZZ verifier_c ;  /* to hold the value of the challenge that the verifier will compute. */
	ZZFromBytes(verifier_c, hash, 64);
	verifier_c = verifier_c % q;
	if (verifier_c >= q) cout<< "The challenge is greater than the prime q.\n";
	/*
	cout <<"Verifier challenge after been hashed:\n";
	cout << verifier_c <<"\n\n";
	*/
	if (verifier_c == proverMessage[2]){
		/* compute the verification equations*/
		ZZ verifier_g_to_lastMessage, verifier_h_to_lastMessage;
		verifier_g_to_lastMessage = MulMod(proverMessage[0], PowerMod(gPublicKey, verifier_c, p), p);
		verifier_h_to_lastMessage = MulMod(proverMessage[1], PowerMod(hPublicKey, verifier_c, p), p);
		if ( (verifier_g_to_lastMessage == PowerMod(g, proverMessage[3], p)) &&
				(verifier_h_to_lastMessage == PowerMod(h, proverMessage[3], p)) ){
			result = true;
		}
	}

	return result;
}



int main(){
	/* Read the parameters q,p,g,h from the file and also compute the secretKey and
	 * the gPublicKey, hPublicKey
	 * */
	readParams();
	initNTLRandom();

	/* if the secretKey will be will be given to us then i'll read it from a file and then pass it
	 * to the prover as a parameter to his function.
	 * */
	//ifstream fin("file that contain the secretKey");
	//if (!fin.good()) {
	//	fprintf(stderr, "couldn't open the secretKey's file.\n");
	//	return 1;
	//}
	/* NOTE: secretKey declared above at global scope. */
	//fin >> secretKey;
	//fin.close();
	//if (secretKey >= p) {
	//	fprintf(stderr, "secret Key was not chosen from the set (1 ... P-1)!\n");
	//	return -1;
	//}


	/* same for if we'll get the values of gPublicKey = g^secretKey,
	 * hPublicKey = h^secretKey from a file
	 *  */
	//ifstream fin("file that contain gPublicKey and hPublicKey");
	//if (!fin.good()) {
	//	fprintf(stderr, "couldn't open the secretKey's file.\n");
	//	return 1;
	//}
	/* NOTE: secretKey declared above at global scope. */
	//fin >> gPublicKey >> hPublicKey;
	//fin.close();
	//if (gPublicKey >= p) {
	//	fprintf(stderr, "g^SecretKey mod p is greater than p!\n");
	//	return -1;
	//}
	//if (gPublicKey >= p) {
	//	fprintf(stderr, "g^SecretKey mod p is greater than p!\n");
	//	return -1;
	//}


	vector<ZZ> verification;
	verification.clear();
	verification = Prover();
	bool result = false;
	result = Verifier(verification);
	cout << result << "\n";
	if (result){
		cout << "The two discrete logarithm have the same secret and are equal!\n";
	}else{
		cout << "The two discrete logarithm have different secret and are not equal!\n";
	}

  	return 0;
}































